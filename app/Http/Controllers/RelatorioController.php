<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PreMatricula;
use App\Matricula;
use App\Renovacao;
use App\Http\Requests\RenovacaoRequest;
use Illuminate\Support\Facades\Gate;
class RelatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
		
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>'','titulo'=>'Relatórios'],    
        ];
        return view('dashboard.relatorios.index', compact('caminhos'));
    }

    public function listaPrematricula(){
		
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $prematriculas = PreMatricula::all();
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
            ['url'=>'','titulo'=>'Lista de Cadastrados Pré-Matricula'],
            
        ];
        return view('dashboard.relatorios.listaprematricula',compact('prematriculas','caminhos'));
    }

    public function listaMatricula(){
		
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $matriculas = Matricula::all();
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
            ['url'=>'','titulo'=>'Lista de Alunos Matriculados'],
            
        ];
        return view('dashboard.relatorios.listamatricula',compact('matriculas','caminhos'));
    }

    public function listaRenovacao(){
		
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $renovacao = Renovacao::all();
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
            ['url'=>'','titulo'=>'Lista de Renovações Solicitadas'],
            
        ];
        return view('dashboard.relatorios.listarenovacao',compact('renovacao','caminhos'));
    }

    public function buscaMatricula(Request $request){
		
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
            ['url'=>route('relatorios.lista.matriculados'),'titulo'=>'Lista de Alunos Matriculados'],
            ['url'=>'','titulo'=>'Pesquisar Nome dos Alunos'],
            
        ];
        
        
        $str = $request->get('buscar');
        $matriculados = Matricula::where( 'nomealuno' , 'ILIKE' , '%'. $str .'%' )
            ->orderBy('nomealuno','asc')
            ->paginate(4);
        return view('dashboard.relatorios.listamatricula', compact('caminhos'))->with([ 'matriculas' => $matriculados ,'buscar' => true ]);   
    }

    public function BuscarAluno(Request $request){
       
		if(Gate::denies('relatorios-view')){
            return view('dashboard.sempermissao');
        }
		
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
            ['url'=>route('relatorios.lista'),'titulo'=>'Lista de Cadastrados Pré-Matricula'],
            ['url'=>'','titulo'=>'Pesquisar Nome do Aluno Sorteado'],
            
        ];
        
        
        $str = $request->get('buscar');
        $prematriculas = PreMatricula::where( 'nomealuno' , 'ILIKE' , '%'. $str .'%' )
            ->orderBy('nomealuno','asc')
            ->paginate(4);
        return view('dashboard.relatorios.listaprematricula', compact('caminhos'))->with([ 'prematriculas' => $prematriculas ,'buscar' => true ]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RenovacaoRequest $request, $id)
    {
        if(Gate::denies('relatorios-edit')){
            return view('dashboard.sempermissao');
        }
		
        Renovacao::find($id)->update($request->all());
        
        return redirect()->route('relatorio.renovacao.lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editrenovacao($id)
    {	
	
		$caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('relatorios.index'),'titulo'=>'Relatórios'],
			['url'=>route('relatorio.renovacao.lista'),'titulo'=>'Lista de Renovações'],
            ['url'=>'','titulo'=>'Editar Renovação'],
            
        ];
		
		if(Gate::denies('relatorios-edit')){
            return view('dashboard.sempermissao');
        }
		
        $user = Auth()->user();
        $renovacao = Renovacao::find($id);

        return view('dashboard.renovacao.editar',compact('renovacao', 'caminhos'));
    }

    public function destroy($id)
    {
		if(Gate::denies('relatorios-delete')){
            return view('dashboard.sempermissao');
        }
		
        PreMatricula::find($id)->delete();
        return redirect()->route('relatorios.lista');
    }
    

    public function renovacaoDestroy($id)
    {
		if(Gate::denies('relatorios-delete')){
            return view('dashboard.sempermissao');
        }
		
        Renovacao::find($id)->delete();
        return redirect()->route('relatorio.renovacao.lista');
    }
}
