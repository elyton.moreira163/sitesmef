<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matricula;
use App\User;
use App\Http\Requests\MatriculaRequest;
use Illuminate\Support\Facades\Gate;
class MatriculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Gate::denies('matricula-view')){
            return view('dashboard.sempermissao');
        }
        
        $caminhos = [
        ['url'=>'/admin','titulo'=>'Tela Principal'],
        ['url'=>'','titulo'=>'Matricula'],
        
        ];
        
        return view('dashboard.matricula.telaprincipal',compact('caminhos'));
        
    }
    public function lista(){
        if(Gate::denies('matricula-view')){
            return view('dashboard.sempermissao');
        }
		
        $matriculas = Matricula::all();
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('matricula.index'),'titulo'=>'Matricula'],
            ['url'=>'','titulo'=>'Lista de Matriculados'],
            
        ];
        return view('dashboard.matricula.lista',compact('matriculas','caminhos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {	
        if(Gate::denies('matricula-edit')){
            return view('dashboard.sempermissao');
        }
		
		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('matricula.index'),'titulo'=>'Matricula'],
			['url'=>'','titulo'=>'Cadastrar Matricula'],
        
        ];
        return view('dashboard.matricula.cadastro', compact('caminhos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatriculaRequest $request)
    {   
        if(Gate::denies('matricula-edit')){
            return view('dashboard.sempermissao');
        }
		
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('matricula.index'),'titulo'=>'Matricula'],
            ['url'=>'','titulo'=>'Inscricao Realizada'],
            
        ];
		
        $user = Auth()->user();
        $user = User::where('email', $request->user_id)->get();
        if(isset($user)){
        $dados = $request->all();
        $dados['user_id'] = $user[0]->id;
        
        
        $matricula = Matricula::create($dados);
        
        //return'Cadastrado com Sucesso......';
        return view('dashboard.matricula.confirmacao');
        //return redirect('/admin/matricula')->with('info','Cadastro feito!');

        //dd($dados);
        }
    }
    public function vermais($id){
		
        if(Gate::denies('matricula-view')){
            return view('dashboard.sempermissao');
        }
		
		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('matricula.index'),'titulo'=>'Matricula'],
			['url'=>route('matricula.lista'),'titulo'=>'Lista de Matriculados'],
			['url'=>'','titulo'=>'Detalhes da Matricula'],
        
        ];
		
        $matricula = Matricula::find($id);
        return view('dashboard.matricula.vermais',compact('matricula','caminhos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {	
        if(Gate::denies('matricula-edit')){
            return view('dashboard.sempermissao');
        }
		
        $user = Auth()->user();
        $matricula = Matricula::find($id);
		
		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('matricula.index'),'titulo'=>'Matricula'],
			['url'=>route('matricula.lista'),'titulo'=>'Lista de Matriculados'],
			['url'=>'','titulo'=>'Editar Matricula'],
        
        ];
		
        return view('dashboard.matricula.editar',compact('matricula','caminhos'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MatriculaRequest $request, $id)
    {
        if(Gate::denies('matricula-update')){
            return view('dashboard.sempermissao');
        }
		
        Matricula::find($id)->update($request->all());
        return redirect()->route('matricula.lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('matricula-delete')){
            return view('dashboard.sempermissao');
        } 
		
        Matricula::find($id)->delete();
        return redirect()->route('matricula.lista');
    }

    public function busca(Request $request){
        
		if(Gate::denies('matricula-view')){
            return view('dashboard.sempermissao');
        } 
		
        /*
        $alunos = Matricula::where('nomealuno', 'LIKE', '%' . $request->Buscar . '%')->get();

        return view('matricula.lista',[
            'alunos'=>$alunos,
            'Buscar' =>$request->Buscar]);

         */

        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('matricula.index'),'titulo'=>'Matricula'],
            ['url'=>route('matricula.lista'),'titulo'=>'Lista de Alunos Matriculados'],
            ['url'=>'','titulo'=>'Pesquisar nome dos Alunos'],
            
        ];
        
        
        $str = $request->get('buscar');
        //$option = $request->input('nomealuno');
        $matriculados = Matricula::where( 'nomealuno' , 'ILIKE' , '%'. $str .'%' )
            ->orderBy('nomealuno','asc')
            ->paginate(4);
        //dd($matriculados);
        return view('dashboard.matricula.lista', compact('caminhos'))->with([ 'matriculas' => $matriculados ,'buscar' => true ]);   
    }
}
