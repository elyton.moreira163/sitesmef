<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anotacao;
use App\Http\Requests\AnotacaoRequest;
use Illuminate\Support\Facades\Gate;
class AnotacaoController extends Controller
{
    public function index(){
        if(Gate::denies('anotacao-view')){
            return view('dashboard.sempermissao');
        } 
		
		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>'','titulo'=>'Anotação'],
        
        ];
        return view('dashboard.anotacao.index',compact('caminhos'));
    }

    public function create(){
        
        if(Gate::denies('anotacao-edit')){
            return view('dashboard.sempermissao');
        } 

		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('anotacao.index'),'titulo'=>'Anotações'],
			['url'=>'','titulo'=>'Cadastrar Anotação'],
        
        ];
        return view('dashboard.anotacao.adicionar',compact('caminhos'));
    }
    
    public function store(AnotacaoRequest $request)
    {   
        if(Gate::denies('anotacao-edit')){
            return view('dashboard.sempermissao');
        }
		
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('anotacao.index'),'titulo'=>'Anotações'],
            ['url'=>'','titulo'=>'Anotação Cadastrada com Sucesso'],
            
        ];

        $user = Auth()->user();
        $dados = $request->all();

        $anotacoes = Anotacao::create($dados);
        return view('dashboard.anotacao.index');
          
    }

    public function listaAnotacao(){
        if(Gate::denies('anotacao-view')){
            return view('dashboard.sempermissao');
        } 
		
        $anotacoes = Anotacao::all();
        $caminhos = [
            ['url'=>'/admin','titulo'=>'Tela Principal'],
            ['url'=>route('anotacao.index'),'titulo'=>'Anotação'],
            ['url'=>'','titulo'=>'Lista de Anotações'],
            
        ];
        return view('dashboard.anotacao.lista',compact('anotacoes','caminhos'));
    }

    public function edit($id)
    {
        if(Gate::denies('anotacao-edit')){
            return view('dashboard.sempermissao');
        } 
		
        $user = Auth()->user();
        $anotacao = Anotacao::find($id);
		
		$caminhos = [
			['url'=>'/admin','titulo'=>'Tela Principal'],
			['url'=>route('anotacao.listar'),'titulo'=>'Anotações'],
			['url'=>'','titulo'=>'Editar Anotação'],
        
        ];	
        return view('dashboard.anotacao.editar',compact('anotacao','caminhos'));
    }

    public function update(AnotacaoRequest $request, $id)
    {
        if(Gate::denies('anotacao-edit')){
            return view('dashboard.sempermissao');
        } 
		
        Anotacao::find($id)->update($request->all());
        return redirect()->route('anotacao.listar');
    }

    public function destroy($id)
    {
        if(Gate::denies('anotacao-delete')){
            return view('dashboard.sempermissao');
        }
		
        Anotacao::find($id)->delete();
        return redirect()->route('anotacao.listar');
    }
}
