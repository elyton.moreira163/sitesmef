<?php

use Illuminate\Database\Seeder;
use App\Permissao;
class PermissaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios1 = Permissao::firstOrCreate([
            'nome' =>'usuario-view',
            'descricao' =>'Acesso a lista de Usuários'
        ]);
        $usuarios2 = Permissao::firstOrCreate([
            'nome' =>'usuario-create',
            'descricao' =>'Adicionar Usuários'
        ]);
        $usuarios2 = Permissao::firstOrCreate([
            'nome' =>'usuario-edit',
            'descricao' =>'Editar Usuários'
        ]);
        $usuarios3 = Permissao::firstOrCreate([
            'nome' =>'usuario-delete',
            'descricao' =>'Deletar Usuários'
        ]);
  
        $papeis1 = Permissao::firstOrCreate([
            'nome' =>'papel-view',
            'descricao' =>'Listar Papéis'
        ]);
        $papeis2 = Permissao::firstOrCreate([
            'nome' =>'papel-create',
            'descricao' =>'Adicionar Papéis'
        ]);
        $papeis3 = Permissao::firstOrCreate([
            'nome' =>'papel-edit',
            'descricao' =>'Editar Papéis'
        ]);
  
        $papeis4 = Permissao::firstOrCreate([
            'nome' =>'papel-delete',
            'descricao' =>'Deletar Papéis'
        ]);

        $matricula1 = Permissao::firstOrCreate([
            'nome' =>'matricula-view',
            'descricao' =>'Adicionar matricula' 
        ]); 

        $matricula2 = Permissao::firstOrCreate([
            'nome' =>'matricula-edit',
            'descricao' =>'Editar matricula' 
        ]); 

        $matricula3 = Permissao::firstOrCreate([
            'nome' =>'matricula-update',
            'descricao' =>'Atualizar matricula' 
        ]); 

        $matricula4 = Permissao::firstOrCreate([
            'nome' =>'matricula-delete',
            'descricao' =>'Deletar matricula' 
        ]); 
        
        $perfil1 = Permissao::firstOrCreate([
            'nome' =>'perfil-view',
            'descricao' =>'Acesso ao Perfil.'
        ]);
          
        $perfil2 = Permissao::firstOrCreate([
            'nome' =>'perfil-edit',
            'descricao' =>'Editar Perfil.'
        ]);

        $prematricula1 = Permissao::firstOrCreate([
            'nome' =>'prematricula-view',
            'descricao' =>'Acessar lista de prematricula' 
        ]);
		
		$prematricula2 = Permissao::firstOrCreate([
            'nome' =>'prematricula-edit',
            'descricao' =>'Editar prematricula' 
        ]);
		
		$prematricula3 = Permissao::firstOrCreate([
            'nome' =>'prematricula-delete',
            'descricao' =>'Acessar lista de prematricula' 
        ]);

        $renovacao1 = Permissao::firstOrCreate([
            'nome' =>'renovacao-view',
            'descricao' =>'Acessar lista de renovacoes' 
        ]);

        $renovacao2 = Permissao::firstOrCreate([
            'nome' =>'renovacao-edit',
            'descricao' =>'Editar renovação' 
        ]);

        $renovacao3 = Permissao::firstOrCreate([
            'nome' =>'renovacao-delete',
            'descricao' =>'Deletar renovação' 
        ]);

        $relatorio1 = Permissao::firstOrCreate([
            'nome' =>'relatorios-view',
            'descricao' =>'Adicionar relatorios' 
        ]);

        $relatorio2 = Permissao::firstOrCreate([
            'nome' =>'relatorios-edit',
            'descricao' =>'Editar relatorios' 
        ]);

        $relatorio3 = Permissao::firstOrCreate([
            'nome' =>'relatorios-delete',
            'descricao' =>'Excluir relatorios' 
        ]);
		
		$anotacao1 = Permissao::firstOrCreate([
            'nome' =>'anotacao-view',
            'descricao' =>'Adicionar anotacao ' 
        ]);
		
		$anotacao2 = Permissao::firstOrCreate([
            'nome' =>'anotacao-edit',
            'descricao' =>'editar anotacao' 
        ]);
		
		$anotacao3 = Permissao::firstOrCreate([
            'nome' =>'anotacao-delete',
            'descricao' =>'excluir anotacao' 
        ]);

        

        echo "Registros de Permissoes criados no sistema";
    }
}
